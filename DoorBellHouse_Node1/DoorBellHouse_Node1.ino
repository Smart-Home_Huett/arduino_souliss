
/***************************************************************************
 * 
 * DoorBell1 Node (Slave)
 
 *
 * Run this code on one of the following boards:
 *  - Arduino with RS485 transceiver
 *  
 *  Wirring:
 *   TX to RS485
 *    RX to RS485
 *    Pin7 To RS485
 *    
 *    Pin 8 Button1
 *    Pin 2 Button2 not implemented
 * 
 *    LED1/2:
 *    Pin3
 *    PIN5
 *    Pin6
 * 
 *    RFID
 *    PIN11
 *    PIN12
 *    PIN13
 *    PIN10
 *    PIn9
 * 
 *    A0 Photosensor
 *    
 *  
 ****************************************************************************
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 ***************************************************************************/

// Let the IDE point to the Souliss framework
#include "SoulissFramework.h"

// Configure the framework
#include "bconf/StandardArduino.h"          // Use a standard Arduino
#include "conf/usart.h"                     // USART RS485

/*************/
// Use the following if you are using an RS485 transceiver with
// transmission enable pin, otherwise delete this section.
#define USARTDRIVER_INSKETCH
#define USART_TXENABLE          1
#define USART_TXENPIN           7
#define USARTDRIVER             Serial
/*************/

// Include framework code and libraries
#include <SPI.h>

// Inlude RFID and Config
#include <MFRC522.h>
#define SS_PIN 10
#define RST_PIN 9
MFRC522 rfid(SS_PIN, RST_PIN); // Instance of the class
MFRC522::MIFARE_Key key;

// Include and Configure DHT11 SENSOR
#include "DHT.h"
#define DHTPIN 4
#define DHTTYPE DHT11   // DHT 11 
//#define DHTTYPE DHT22   // DHT 22  (AM2302)
//#define DHTTYPE DHT21   // DHT 21 (AM2301)
DHT dht(DHTPIN, DHTTYPE);

/*** All configuration includes should be above this line ***/
#include "Souliss.h"

// Define the RS485 network configuration
#define myvNet_subnet     0xFF00
#define myvNet_supern     0x0000
#define myvNet_supern_E1  Gateway_RS485

//Define RS485 Network
#define Node0_Bridge1_RS485     0xCE01  //Gateway Note for RS-485 #1
#define Peer_RS485_Node1  0xCE02  //Node1 Bell on house
#define Peer_RS485_Node2  0xCE03  //Node Bell on street

//NODE Definitions
#define Master_NODE         0
#define NODE1               0
#define NODE2               0
#define ANALOGDAQ           2           
#define DEADBAND            0.05        
#define LEDCONTROL          6               
#define LEDRED              7               
#define LEDGREEN            8             
#define LEDBLUE             9
#define TEMPERATURE         10
#define HUMIDITY            12

//Topic Deffinition
#define BrightWatherStation 0x0006,0x01
#define DarkWatherStation   0x0006,0x02
#define BrightDoor1 0x0006,0x03
#define DarkDoor1   0x0006,0x04
#define BrightDoor12 0x0006,0x05
#define DarkDoor2   0x0006,0x06

//Topic Deffinition Bells
#define Bell1 0x0005,0x01
#define Bell2 0x0005,0x02
#define Bell3 0x0005,0x03
#define Bell4 0x0005,0x04

//Topic Deffinition RFID
#define RFID 0x0007, 0x01

//Input Definitions
#define INPUTPIN1 2
#define INPUTPIN2 8
//Output Definitions
//Define LED1
#define LED1RED 3
#define LED1GREEN 5
#define LED1BLUE 6

void setup() {
	Initialize();

	// Set network parameters
	SetAddress(Peer_RS485_Node1, myvNet_subnet, Node0_Bridge1_RS485);

	// Set Componets
	Set_LED_Strip(LEDCONTROL);             // Set a logic to control a LED strip
	Set_T54(ANALOGDAQ);
	Set_Temperature(TEMPERATURE);
	Set_Humidity(HUMIDITY);

	// Set the typical logic to handle the Bell and light
	// Define inputs, outputs pins
	pinMode(INPUTPIN1, INPUT);                  // Hardware pulldown required
	pinMode(INPUTPIN2, INPUT);                  // Hardware pulldown required

	// LED OUTPUTS
	initLED();

	/*Setup RFID*/
	SPI.begin(); // Init SPI bus
	rfid.PCD_Init(); // Init MFRC522
	dht.begin();

	for (byte i = 0; i < 6; i++) {
		key.keyByte[i] = 0xFF;
	}

}

void loop() {
	EXECUTEFAST()
	{
		UPDATEFAST();

		// Run some logics here
		FAST_110ms()
		{
			// read the state of the pushbutton value:
			// buttonState1 = ;
			// buttonState2 = digitalRead(INPUTPIN2);

			if (digitalRead(INPUTPIN1) == HIGH) {
				pblsh(Bell1);
				bellresponse(1);
			}
			if (0/*digitalRead(INPUTPIN2) == HIGH*/) {
				pblsh(Bell2);
				bellresponse(1);
			}

			// Execute the logic that handle the LED
			Logic_LED_Strip(LEDCONTROL);
			// Use the output values to control the PWM
			analogWrite(3, mOutput(LEDRED));
			analogWrite(5, mOutput(LEDGREEN));
			analogWrite(6, mOutput(LEDBLUE));

			// Just process communication as fast as the logics
			ProcessCommunication();
		}
		FAST_710ms()
		{
			readRFID();
		}
		FAST_7110ms()
		{
			Read_AnalogIn(ANALOGDAQ);
			Logic_Temperature(TEMPERATURE);
			Logic_Humidity(HUMIDITY);
		}
		// Here we handle here the communication with Android, commands and notification
		FAST_PeerComms();

	}

	EXECUTESLOW()
	{
		UPDATESLOW();
		SLOW_10s()
		{
			Souliss_ReadDHT(TEMPERATURE, HUMIDITY);
		}

		SLOW_710s()
		{
			// The timer handle timed-on states
			Timer_LED_Strip(LEDCONTROL);

		}
	}
	EXECUTESPEEDY()
	{
		UPDATESPEEDY();

		// Execute the code every 3 times of free MCU
		SPEEDY_x(3)
		{

			// Acquire data from the microcontroller ADC
			AnalogIn(A0, ANALOGDAQ, 0.09, 0); // The raw data is 0-1024, scaled as 0-100% without bias (100 / 1024 = 0.09)
		}

	}
}

void bellresponse(int bell) {
	if (bell == 1) {
		setColor(255, 0, 0, LED1RED, LED1GREEN, LED1BLUE);
		delay(1000);
		resetColor(LED1RED, LED1GREEN, LED1BLUE);
	} else {
		//Something wrong
	}

}

void setColor(int red, int green, int blue, int redPin, int greenPin,
		int bluePin) {
#ifdef COMMON_ANODE
	red = 255 - red;
	green = 255 - green;
	blue = 255 - blue;
#endif
	analogWrite(redPin, red);
	analogWrite(greenPin, green);
	analogWrite(bluePin, blue);
}
void resetColor(int redPin, int greenPin, int bluePin) {
	analogWrite(redPin, mOutput(LEDRED));
	analogWrite(greenPin, mOutput(LEDGREEN));
	analogWrite(bluePin, mOutput(LEDBLUE));

}

void readRFID() {
	/*RFID Tag REader*/
	if (rfid.PICC_IsNewCardPresent()) {
		if (rfid.PICC_ReadCardSerial()) {
			MFRC522::PICC_Type piccType = rfid.PICC_GetType(rfid.uid.sak);
			if (piccType == MFRC522::PICC_TYPE_MIFARE_MINI
					|| piccType == MFRC522::PICC_TYPE_MIFARE_1K
					|| piccType == MFRC522::PICC_TYPE_MIFARE_4K) {
				//Send UID TO Master
				uint8_t mypayload[4];
				for (byte i = 0; i < 4; i++) {
					mypayload[i] = rfid.uid.uidByte[i];
				}
				if (mypayload != NULL) {
					pblshdata(RFID, mypayload, 4);
					bellresponse(1);
				}
			}
		}
	}
}

void Souliss_ReadDHT(uint8_t TEMPERATURE_SLOT, uint8_t HUMIDITY_SLOT) {
  float h,t;
  if(!isnan(h=h = dht.readHumidity())){
    Souliss_ImportAnalog(memory_map, HUMIDITY_SLOT, &h);
  }
  // Read temperature as Celsius
  if (!isnan(t = dht.readTemperature())) {
    Souliss_ImportAnalog(memory_map, TEMPERATURE_SLOT, &t);
    //Something Wrong
  }
}

void initLED() {
	pinMode(LED1RED, OUTPUT);
	pinMode(LED1GREEN, OUTPUT);
	pinMode(LED1BLUE, OUTPUT);
}


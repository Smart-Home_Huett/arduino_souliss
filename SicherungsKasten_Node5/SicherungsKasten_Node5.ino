
/* Program to read a Ferraris electrical meter using a IR light sensor.
 * Comication via RS485. Useing Souliss Framework to comunicate to over divices.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
// Let the IDE point to the Souliss framework
#include "SoulissFramework.h"

// Configure the framework
#include "bconf/StandardArduino.h"          // Use a standard Arduino
#include "conf/usart.h"                     // USART RS485
//#include <avr/eeprom.h>
#include <math.h>
#include "limits.h"                         // _MAX defines
/*************/
// Use the following if you are using an RS485 transceiver with
// transmission enable pin, otherwise delete this section.

#define USARTDRIVER_INSKETCH
#define USART_TXENABLE          1
#define USART_TXENPIN           3
#define USARTDRIVER             Serial
/*************/

/*** All configuration includes should be above this line ***/
#include "Souliss.h"

// Define the RS485 network configuration
#define myvNet_subnet     0xFF00
#define myvNet_supern     0x0000
#define myvNet_supern_E1  Gateway_RS485

//Define RS485 Network
#define Node0_Bridge1_RS485     0xCE01  //Gateway Note for RS-485
#define Peer_RS485_Node1        0xCE02  //Node1 Bell on house
#define Peer_RS485_Node2        0xCE03  //Node Bell on street
#define Peer_RS485_Node5        0xCE04  //Node Elektro

//Define Pins
#define OUTPUT_IRLED 11
#define OUTPUT_STATUSLED 13
#define INPUT_ANALOGE_IR A0

//NODE Definitions
#define POWER 0

// Constants for Calculating
#define SENSORDELAY 50
#define ROUNDSPERKWH 75 
//Varialbes for Calculating POWER
int trigerValue;
int lowerValue = 0;
int rotation = 0;
int roundsPerW;
float wattH = 0;
unsigned long rotationTimeOld = 0;
unsigned long rotationTimeNew = 0;


//to enable DEBUG 1
#define DEBUG 0


void setup() {
  Initialize();

  // Set network parameters
  if (!DEBUG) {
    SetAddress(Peer_RS485_Node5, myvNet_subnet, Node0_Bridge1_RS485);
  } else {
    Serial.begin(9600);
  }
  //init Ouptut Pins
  pinMode(OUTPUT_IRLED, OUTPUT);
  pinMode(OUTPUT_STATUSLED, OUTPUT);
  delay(500);
  trigerValue = initTrigger();
  roundsPerW = initRoundsPerW();
  digitalWrite(OUTPUT_STATUSLED, HIGH);
   // Set Componets
    Set_T57(POWER);


}

void loop() {
  EXECUTEFAST() {
    UPDATEFAST();
    FAST_70ms() {
      checkSensor();
    }
    // Here we handle here the communication with Android, commands and notification
    FAST_PeerComms();

  }
}

void checkSensor() {
  // 
  if (readSensor() >= trigerValue && lowerValue == 0) {
    lowerValue = 1;
  } else if (readSensor() <= trigerValue && lowerValue == 1) {
    rotationTimeNew = millis();
    rotation++;
    callculateWatt();
    lowerValue = 0;
  } else {
    //?
  }
}

int readSensor() {
  int sensorValueOn;
  digitalWrite(OUTPUT_IRLED, HIGH);
  delay(SENSORDELAY);
  // read the analog in value:
  sensorValueOn = analogRead(INPUT_ANALOGE_IR);
  return sensorValueOn;
}

// Initilize Trigger Value
int initTrigger() {
  if (DEBUG) {
    Serial.println("Start Triger Funktion");
  }
  bool complete = 0;
  int maxValue = 0;
  int minValue = 10000;
  int tmpValue;
  while ( ( (maxValue - minValue) <= maxValue / 3 ) ) {
    tmpValue = readSensor();
    if (DEBUG) {
      Serial.println(tmpValue);
    }
    if (tmpValue > maxValue) maxValue = tmpValue;
    if (tmpValue < minValue) minValue = tmpValue;
  }
  trigerValue = (maxValue - minValue) / 30 + minValue;
  if (DEBUG) {
    Serial.print("Triger Value: ");
    Serial.println(trigerValue - 5);
  }
  return trigerValue - 5;
}
//Calculation for Rounds per Watt
int initRoundsPerW() {
  return (1000.00/ROUNDSPERKWH)*3600;
}

void callculateWatt() {
  int watt;
  //IF Timer resets after 50 Days 
  if (rotationTimeNew < rotationTimeOld) {
    //
    watt = (int)(roundsPerW/((ULONG_MAX - rotationTimeOld + rotationTimeNew)/1000));
    rotationTimeOld = rotationTimeNew;
  } else {
    watt = (int)(roundsPerW/((rotationTimeNew - rotationTimeOld)/1000));
    rotationTimeOld = rotationTimeNew;
    if (DEBUG) {
      Serial.print("Watt: ");
      Serial.println(watt);
    }
  }
  setPower(watt);
  Logic_Power(POWER);
}

void setPower(int setwatt) {
  float watt;
  if (isnan(setwatt)) {
    //Something Wrong
    watt = -1;
  } else {
    watt = (float) setwatt;
  }
  Souliss_ImportAnalog(memory_map, POWER, &watt);
}

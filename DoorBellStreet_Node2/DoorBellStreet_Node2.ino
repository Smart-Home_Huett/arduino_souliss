
/***************************************************************************
 * 
 * DoorBell2 Node (Slave)
 
 *
 * Run this code on one of the following boards:
 *  - Arduino with RS485 transceiver
 *  
 *  Wirring:
 *   TX to RS485
 *    RX to RS485
 *    Pin7 To RS485
 *    
 *    Pin 8 Button1
 *    Pin 2 Button2 not implemented
 * 
 *    LEDs1: Line 108-110
 *    Pin3 = RED
 *    PIN6 = GREEN
 *    Pin5 = BLUE

 *    LEDs2: Line 112-114
 *    Pin9 = RED
 *    Pin11 = GREEN
 *    Pin10 = BLUE
 *    
 *  
 ****************************************************************************
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 ***************************************************************************/

// Let the IDE point to the Souliss framework
#include "SoulissFramework.h"

// Configure the framework
#include "bconf/StandardArduino.h"          // Use a standard Arduino
#include "conf/usart.h"                     // USART RS485

/*************/
// Use the following if you are using an RS485 transceiver with
// transmission enable pin, otherwise delete this section.
#define USARTDRIVER_INSKETCH
#define USART_TXENABLE          1
#define USART_TXENPIN           13
#define USARTDRIVER             Serial
/*************/

// Include framework code and libraries
//#include <SPI.h>
// Include and Configure DHT22 SENSOR
#include "DHT.h"
#define DHTPIN 12
//#define DHTTYPE DHT11   // DHT 11
#define DHTTYPE DHT22   // DHT 22  (AM2302)
//#define DHTTYPE DHT21   // DHT 21 (AM2301)
DHT dht(DHTPIN, DHTTYPE);

/*** All configuration includes should be above this line ***/
#include "Souliss.h"

// Define the RS485 network configuration
#define myvNet_subnet     0xFF00
#define myvNet_supern     0x0000
#define myvNet_supern_E1  Gateway_RS485

//Define RS485 Network
#define Node0_Bridge1_RS485     0xCE01  //Gateway Note for RS-485 #1
#define Peer_RS485_Node1  0xCE02  //Node1 Bell on house
#define Peer_RS485_Node2  0xCE03  //Node Bell on street

/*NODE Definitions*/
#define Master_NODE         0
#define NODE1               0
#define NODE2               0
#define NODE2LEDCONTROL1         3
#define NODE2LEDRED1             4
#define NODE2LEDGREEN1           5
#define NODE2LEDBLUE1            6
#define NODE2LEDCONTROL2         7
#define NODE2LEDRED2             8
#define NODE2LEDGREEN2           9
#define NODE2LEDBLUE2            10
#define TEMPERATURE         12
#define HUMIDITY            14

//Topic Deffinition of BUS
#define BrightWatherStation 0x0006,0x01
#define DarkWatherStation   0x0006,0x02
#define BrightDoor1 0x0006,0x03
#define DarkDoor1   0x0006,0x04
#define BrightDoor12 0x0006,0x05
#define DarkDoor2   0x0006,0x06

//Topic Deffinition Bells
#define Bell1 0x0005,0x01
#define Bell2 0x0005,0x02
#define Bell3 0x0005,0x03
#define Bell4 0x0005,0x04

//Input Definitions
#define INPUTPIN1 8
#define INPUTPIN2 2
//Output Definitions
//Define LED1
#define LED1RED 3
#define LED1GREEN 6
#define LED1BLUE 5
//Define LED2
#define LED2RED 9
#define LED2GREEN 11
#define LED2BLUE 10

void setup() {
	Initialize();

	// Set network parameters
	SetAddress(Peer_RS485_Node2, myvNet_subnet, Node0_Bridge1_RS485);

	// Set Componets
	Set_LED_Strip(NODE2LEDCONTROL1);       // Set a logic to control a LED strip
	Set_LED_Strip(NODE2LEDCONTROL2);
  Set_Temperature(TEMPERATURE);
  Set_Humidity(HUMIDITY);


	// Define inputs, outputs pins
	pinMode(INPUTPIN1, INPUT);                  // Hardware pulldown required
	pinMode(INPUTPIN2, INPUT);                  // Hardware pulldown required

	//Init LEDs
	initLED();
}

void loop() {
	EXECUTEFAST()
	{
		UPDATEFAST();

		// Run some logics here
		FAST_110ms()
		{
			//Check Button 1
			if (digitalRead(INPUTPIN1) == HIGH) {
				pblsh(Bell1);
				/* analogWrite(255, mOutput(NODE2LEDRED1));
				 delay(1000);
				 analogWrite(0, mOutput(NODE2LEDRED1));*/
				bellresponse(1);
			}
			//Check Button 2
			if (digitalRead(INPUTPIN2) == HIGH) {
				pblsh(Bell2);
				/*analogWrite(255, mOutput(NODE2LEDRED2));
				 delay(1000);
				 analogWrite(0, mOutput(NODE2LEDRED2));*/
				bellresponse(2);
			}

			// Execute the logic that handle the LED
			Logic_LED_Strip(NODE2LEDCONTROL1);
			// Use the output values to control the PWM
			analogWrite(3, mOutput(NODE2LEDRED1));
			analogWrite(5, mOutput(NODE2LEDGREEN1));
			analogWrite(6, mOutput(NODE2LEDBLUE1));

			// Execute the logic that handle the LED
			Logic_LED_Strip(NODE2LEDCONTROL2);
			// Use the output values to control the PWM
			analogWrite(9, mOutput(NODE2LEDRED2));
			analogWrite(10, mOutput(NODE2LEDGREEN2));
			analogWrite(11, mOutput(NODE2LEDBLUE2));

			// Just process communication as fast as the logics
			ProcessCommunication();
		}
    FAST_7110ms()
    {
      Logic_Temperature(TEMPERATURE);
      Logic_Humidity(HUMIDITY);
    }
   
		// Here we handle here the communication with Android, commands and notification
		FAST_PeerComms();

	}

	EXECUTESLOW()
	{
		UPDATESLOW();
		SLOW_10s()
		{
			Souliss_ReadDHT(TEMPERATURE, HUMIDITY);
		}
		SLOW_710s()
		{
			// The timer handle timed-on states
			Timer_LED_Strip(NODE2LEDCONTROL1);
			Timer_LED_Strip(NODE2LEDCONTROL2);
		}
	}
}

void bellresponse(int bell) {
	if (bell == 1) {
		setColor(255, 0, 0, LED1RED, LED1GREEN, LED1BLUE);
		delay(1000);
		resetColor(LED1RED, LED1GREEN, LED1BLUE);
	} else if (bell == 2) {
		setColor(255, 0, 0, LED2RED, LED2GREEN, LED2BLUE);
		delay(1000);
		resetColor2(LED2RED, LED2GREEN, LED2BLUE);
	} else {

		//Something wrong
	}

}

void setColor(int red, int green, int blue, int redPin, int greenPin,
		int bluePin) {
#ifdef COMMON_ANODE
	red = 255 - red;
	green = 255 - green;
	blue = 255 - blue;
#endif
	analogWrite(redPin, red);
	analogWrite(greenPin, green);
	analogWrite(bluePin, blue);
}
void resetColor(int redPin, int greenPin, int bluePin) {
	analogWrite(redPin, mOutput(NODE2LEDRED1));
	analogWrite(greenPin, mOutput(NODE2LEDGREEN1));
	analogWrite(bluePin, mOutput(NODE2LEDBLUE1));
}

void resetColor2(int redPin, int greenPin, int bluePin) {
	analogWrite(redPin, mOutput(NODE2LEDRED2));
	analogWrite(greenPin, mOutput(NODE2LEDGREEN2));
	analogWrite(bluePin, mOutput(NODE2LEDBLUE2));
}

void initLED() {
	// LED OUTPUTS
	//LED1
	pinMode(LED1RED, OUTPUT);
	pinMode(LED1GREEN, OUTPUT);
	pinMode(LED1BLUE, OUTPUT);
	//LED2
	pinMode(LED2RED, OUTPUT);
	pinMode(LED2GREEN, OUTPUT);
	pinMode(LED2BLUE, OUTPUT);
}

void Souliss_ReadDHT(uint8_t TEMPERATURE_SLOT, uint8_t HUMIDITY_SLOT) {
  float h,t;
  if(!isnan(h=h = dht.readHumidity())){
    Souliss_ImportAnalog(memory_map, HUMIDITY_SLOT, &h);
  }
  // Read temperature as Celsius
  if (!isnan(t = dht.readTemperature())) {
    Souliss_ImportAnalog(memory_map, TEMPERATURE_SLOT, &t);
    //Something Wrong
  }
}

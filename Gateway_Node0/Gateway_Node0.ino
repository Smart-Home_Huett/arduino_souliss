
/***************************************************************************
 * 
 * Gateway Node (Master)
 * Slaves:
 *        DoorBell1
 *        DoorBell2
 *        Ferraris electrical meter
 *
 * Run this code on one of the following boards:
 *  - Arduino Mega with Ethernet Shield (W5100) and with RS485 transceiver
 *
 * As option you can run the same code on the following, just changing the
 * relevant configuration file at begin of the sketch
 *  - Arduino with ENC28J60 Ethernet Shield with RS485 transceiver
 *  - Arduino with W5200 Ethernet Shield with RS485 transceiver
 *  - Arduino with W5500 Ethernet Shield with RS485 transceiver
 *  
 ****************************************************************************
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 ***************************************************************************/

// Let the IDE point to the Souliss framework
#include "SoulissFramework.h"

// Configure the framework
#include "bconf/StandardArduino.h"          // Use a standard Arduino
#include "conf/ethW5100.h"                  // Ethernet through Wiznet W5100
#include "conf/usart.h"                     // USART RS485
#include "conf/Gateway.h"                   // The main node is the Gateway

/*************/
// Use the following if you are using an RS485 transceiver with
// transmission enable pin, otherwise delete this section.
#define USARTDRIVER_INSKETCH
#define USART_TXENABLE          1
#define USART_TXENPIN           3
#define USARTDRIVER             Serial
/*************/

// Include framework code and libraries
#include <SPI.h>

/*** All configuration includes should be above this line ***/
#include "Souliss.h"

//RCSwitch for Controlling RF Power Sockets
#include "RCSwitch.h"
RCSwitch mySwitch = RCSwitch();

// Define the network configuration according to your router settings
#define Gateway_address_Node1 7
#define Peer_address_Node3    8
uint8_t ip_address_Gateway[4] = { 0, 0, 0, Gateway_address_Node1 };
uint8_t ip_address_Node3[4] = { 0, 0, 0, Peer_address_Node3 };
uint8_t subnet_mask[4] = { 255, 255, 255, 0 };
uint8_t ip_gateway[4] = { 0, 0, 0, 0 };
#define myvNet_address  ip_address_Gateway[3]       // The last byte of the IP address (77) is also the vNet address

// Define the RS485 network configuration
#define myvNet_subnet     0xFF00
#define myvNet_supern     0x0000
#define myvNet_supern_E1  Gateway_RS485

//Define RS485 Network
#define Gateway_RS485     0xCE01  //Gateway Note for RS-485 #1
#define Peer_RS485_Node1  0xCE02  //Node1 Bell on house
#define Peer_RS485_Node2  0xCE03  //Node Bell on street
#define Peer_RS485_Node5  0xCE04  //Node Elektro

//NODE Definitions
#define LIGHT1_NODE1    0
#define LIGHT2_NODE1    1
#define LIGHT3_NODE1    2
#define LIGHT4_NODE1    3
#define LIGHT5_NODE1    4

//Topic Deffinition
#define BrightWatherStation 0x0006,0x01
#define DarkWatherStation   0x0006,0x02
#define BrightDoor1 0x0006,0x03
#define DarkDoor1   0x0006,0x04
#define BrightDoor12 0x0006,0x05
#define DarkDoor2   0x0006,0x06

//Topic Deffinition Bells
#define Bell1 0x0005,0x01
#define Bell2 0x0005,0x02
#define Bell3 0x0005,0x03
#define Bell4 0x0005,0x04

//Topic Deffinition RFID
#define RFID 0x0007, 0x01

//Output Pins for Raspberry
#define OUTPUT_PIN1 22
#define OUTPUT_PIN2 23

// Send an output command to the socket
#define RcDigOut(code1, code2, value, slot) Souliss_RcDigOut(code1, code2, value, memory_map, slot)
void Souliss_RcDigOut(char* code1, int code2, U8 value, U8 *memory_map,
		U8 slot) {
	// If output is active switch on the pin, else off
	if (memory_map[MaCaco_OUT_s + slot] == value)
		mySwitch.switchOn(code1, code2);
	else
		mySwitch.switchOff(code1, code2);
}

void setup() {
	Initialize();

	// Set network parameters
	Souliss_SetIPAddress(ip_address_Gateway, subnet_mask, ip_gateway);
	SetAsGateway(myvNet_address);      // Set this node as gateway for SoulissApp
	SetAddress(Gateway_RS485, myvNet_subnet, myvNet_supern); // Set the address on the RS485 bus

	// This node as gateway will get data from the Peer
	SetAsPeerNode(Peer_address_Node3, 3);                  //Ethernet GardenPeer

	//RS485 Nodes
	SetAsPeerNode(Peer_RS485_Node1, 1);                    //RS485
	SetAsPeerNode(Peer_RS485_Node2, 2);                    //RS485
	SetAsPeerNode(Peer_RS485_Node5, 4);                    //RS485

	// Set the typical logic to handle
	Set_T11(LIGHT1_NODE1);
	Set_T11(LIGHT2_NODE1);
	Set_T11(LIGHT3_NODE1);
	Set_T11(LIGHT4_NODE1);
	Set_T11(LIGHT5_NODE1);
	// Define inputs, outputs pins and pulldown
	pinMode(OUTPUT_PIN1, OUTPUT);
	pinMode(OUTPUT_PIN2, OUTPUT);

	pinMode(7, OUTPUT);               // trasmettitore 433Mhz

	// Declare the pin where the transmitter is connected
	mySwitch.enableTransmit(7);

}

void loop() {
	// Here we start to play
	EXECUTEFAST()
	{
		UPDATEFAST();

		// Run some logics here
		FAST_110ms()
		{
			// Bell Check
			if (sbscrb(Bell1)) {
				digitalWrite(OUTPUT_PIN1, HIGH);
				delay(1500);
				digitalWrite(OUTPUT_PIN1, LOW);
			}
			if (sbscrb(Bell2)) {
				digitalWrite(OUTPUT_PIN1, HIGH);
				delay(1500);
				digitalWrite(OUTPUT_PIN1, LOW);
			}
			if (sbscrb(Bell3)) {
				digitalWrite(OUTPUT_PIN1, HIGH);
				delay(1500);
				digitalWrite(OUTPUT_PIN1, LOW);
			}
			if (sbscrb(Bell4)) {
				digitalWrite(OUTPUT_PIN2, HIGH);
				delay(1500);
				digitalWrite(OUTPUT_PIN2, LOW);
			}
		}
		FAST_510ms()
		{
			// Execute the logic
			Logic_T11(LIGHT1_NODE1);
			Logic_T11(LIGHT2_NODE1);
			Logic_T11(LIGHT3_NODE1);
			Logic_T11(LIGHT4_NODE1);
			Logic_T11(LIGHT5_NODE1);

			if (isTrigger()) {
				RcDigOut("11101", 1, Souliss_T1n_Coil, LIGHT1_NODE1);
				RcDigOut("11101", 2, Souliss_T1n_Coil, LIGHT2_NODE1);
				RcDigOut("11101", 3, Souliss_T1n_Coil, LIGHT3_NODE1);
				RcDigOut("11101", 4, Souliss_T1n_Coil, LIGHT4_NODE1);
				RcDigOut("11101", 5, Souliss_T1n_Coil, LIGHT5_NODE1);
			}
		}
		FAST_710ms()
		{
			uint8_t RFIDpayload_len;
			uint8_t RFIDpayload[4];
			if (sbscrbdata(RFID, RFIDpayload, &RFIDpayload_len)) {
				//TEST IF 65 C9 10 88
				digitalWrite(OUTPUT_PIN1, HIGH);
				delay(1000);
				digitalWrite(OUTPUT_PIN1, LOW);
				if (RFIDpayload[0] == 0x26 && RFIDpayload[1] == 0xB9
						&& RFIDpayload[2] == 0x30 && RFIDpayload[3] == 0x88) {
					Serial.println("RFID");
					digitalWrite(OUTPUT_PIN2, HIGH);
					delay(1000);
					digitalWrite(OUTPUT_PIN2, LOW);
				}
			}
		}
	}

	// Here we handle here the communication with Android, commands and notification
	FAST_GatewayComms();
	FAST_21110ms()
	pblsh (Notify_GatewayAlive);
}

void writeData() {

}

